var express = require('express');
var router = express.Router();
var passport = require('passport');

var dataClass = require("../config/data.js");
var data = new dataClass();



	/* GET home page. */
	router.get('/', function(req, res) {
		res.render('index', { title: 'Express' });
	});

	router.get('/auth/google', passport.authenticate('google'));

	router.get('/auth/google/return',  passport.authenticate('google', { 
		successRedirect: '/menu',
        failureRedirect: '/signup' 
    }));

	router.get('/signup', function(req, res){
		res.render('signup', {title: 'Signup', message: req.flash('signupMessage')});
	});

	// process the signup form
	router.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/menu', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	router.get('/logout', function(req, res){
		req.logout();
		res.redirect('/login');
	});

	//route middleware to make sure a user is logged in
	function isLoggedIn(req, res, next){

		// If user is authenticated in the session, carry on
		if(req.isAuthenticated()){
			return next();
		}

		//if they aren't redirect to home page
		res.redirect('/login');
	}


module.exports = router;