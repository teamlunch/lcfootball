var express = require('express');
var router = express.Router();
var passport = require('passport');


	router.get('/', isLoggedIn, function(req, res) {
		res.render('menu', { title: 'Menu123' });
	});

	//route middleware to make sure a user is logged in
	function isLoggedIn(req, res, next){

		// If user is authenticated in the session, carry on
		if(req.isAuthenticated()){
			return next();
		}

		//if they aren't redirect to home page
		res.redirect('/login');
	}
	
module.exports = router;
