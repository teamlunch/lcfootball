var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var stylus = require('stylus');
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');

var app = express();




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//For our CSS Preprocessor STYLUS
//Explanation on setup: 
//http://stackoverflow.com/questions/18805637/express-js-stylus-automatic-stylus-compiling-doesnt-work
app.use(stylus.middleware(
  { src: __dirname + '/resources/',
    dest: __dirname + '/public/',
  }
));
app.use('/static', express.static(__dirname + '/public/static'));
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/images', express.static(__dirname + '/public/images'));

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));



app.use(session({secret: "iamsneaky"}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

var routes = require('./routes/index');
var users = require('./routes/users');
var menu = require('./routes/menu');
var login = require('./routes/login');
var passConfig = require('./config/passport');
var data = require('./config/data')

app.use('/', routes);
app.use('/users', users);
app.use('/menu', menu);
app.use('/login', login);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
