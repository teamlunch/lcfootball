var express = require('express');
var router = express.Router();


function Utility () {
	

}




Utility.prototype.createRandomNumber = function() {
	return Math.random().toString(36).substr(2, 4);
}


/****************************
* Creates n number digit alpha numeric random string
****************************/
Utility.prototype.createUID = function() {
	
	// What type of name is that?
	var intLookBack = 7; // Amount of digits to substring the time stamp
	var holdRtrn = ""; // Set as a string in order to concatenate	
	var randomLength = 6; //number of digits to concatenate	
	var randomIndex = Math.floor((Math.random() * randomLength) + 1);
	
	for(var i = 1; i <= randomLength; i++){
		// Used to place the miliseocnds randomly in random number
		if(randomIndex === i) {
			// Gets the time in miliseconds since January 1 1970
			var mili = "" + new Date().getTime();
			// Substring the total miliseconds to get numbers that change
			var intSubString = mili.substr((mili.length - intLookBack), intLookBack);
			holdRtrn = holdRtrn + intSubString;
		}
		else{
			holdRtrn = holdRtrn + this.createRandomNumber();
		}		
	}

//delay function
/*
	var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > 100){
      break;
    }
  }
*/
	return holdRtrn;
}

//TODO: There is a chance if the gods align that this could go forever
/****************************
* Checks and re-creates a new uniqueId for a card
*****************************/
Utility.prototype.checkArrayForDupes = function(arrayToCheck) {
	// Holds unqiue values that will be returned to update the objects
	//	Note: Most likely don't have to return the uniqueArray but I am Java Nerd
	var uniqueArray = [];

	//Used tell if key has been seen before and for debugging purposes and to
	var objectArray = [];

	for(var i = 0; i < arrayToCheck.length; i++) {
		// Use the uniqueId for the key because this is the value that has to be unique
		var key = arrayToCheck[i].uniqueId;

		// Puts the entire card object into value
		var value = arrayToCheck[i];

		// If this is undefined it means the key has never been added before
		if(objectArray[key] === undefined){
			//Array that holds all unique objects	
			uniqueArray.push(value);

			// Increment the counter for debugging and makes sure it isn't undefined
			objectArray[key] = 1;
		}
		else{
			// Increment for debugging purposes
			objectArray[key]++;
			// Get a new Key
			arrayToCheck[i].uniqueId = this.createUID();

			// Recursion used recheck the new value with the rest of the array
			uniqueArray = this.checkArrayForDupes(arrayToCheck);
			

		}
	}

	/**** Used for Debugging *****/
	//console.log("---------- Recusion Run Completed ------- ");
	//console.log(objectArray);
	//console.log(uniqueArray);	
	//console.log("--------- Recusion Run Completed");
	
	return uniqueArray;
}

module.exports = Utility;
